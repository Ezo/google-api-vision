module.exports = function extractvalidWords(games) {
    const validWords = new Set();
    games.forEach(game => 
        game.name.split(' ').forEach(word => 
            validWords.add(word.trim().toLowerCase())));
    return Array.from(validWords);
}