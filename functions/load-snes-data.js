const fs = require('fs');

module.exports = () => 
    fs.readFileSync('./data/snes-games.dat').toString()
    .split('\r\n')
    .map(line => line.split('\t'))
    .map(splitLine => ({ 
        name: splitLine[0],
        company: splitLine[1],
        year: splitLine[2]
    }));
