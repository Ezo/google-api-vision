const vision = require('@google-cloud/vision');
   
const client = new vision.ImageAnnotatorClient({
    keyFile: './Blog-82a748f14fb8.json'
});

async function findLogos(imagePath) {
    const [logoResult] = await client.logoDetection(imagePath);
    return logoResult.logoAnnotations
        .filter(annotation => annotation.score > .5)
        .map(annotation => annotation.description);
}

async function findTexts(imagePath) {
    const [textResult] = await client.textDetection(imagePath);
    return textResult.fullTextAnnotation.text
        .replace(/\n/g, ' ');
}

module.exports = {
    findLogos,
    findTexts
}
