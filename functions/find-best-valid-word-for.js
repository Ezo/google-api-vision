const stringSimilarity = require('string-similarity');
const invalidWords = ['capcom', 'license', 'usa', 'system']

module.exports = function findBestValidWordFor(word, validWords) {
    word = word.trim().toLowerCase();
    if (invalidWords.indexOf(word) > -1) return null;
    const result =  stringSimilarity.findBestMatch(word, validWords);
    const bestMatch = result.bestMatch;
    return bestMatch.rating > .75 ? bestMatch.target : null;
}