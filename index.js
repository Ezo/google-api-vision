const loadSnesData = require('./functions/load-snes-data');
const extractValidWords = require('./functions/extract-valid-words');
const findBestValidWordFor = require('./functions/find-best-valid-word-for');
var stringSimilarity = require('string-similarity');
const fs = require('fs');
const { findLogos, findTexts } = require('./functions/vision-api');

const toleranceInTitle = .4;

async function main() {
    const snesData = loadSnesData();
    const validWords = extractValidWords(snesData);

    const imagesToScan = fs.readdirSync('./images')
        .map(fileName => `./images/${fileName}`);

    for (let currentImage of imagesToScan) {
        console.log(`Scanning file ${currentImage}`);
        if (await isSnesLogoPresent(currentImage)) {
            console.log('  Found SNES logo in image.');
            let allTexts = await findTexts(currentImage);
            allTexts = allTexts
                .split(' ')
                .map(word => findBestValidWordFor(word, validWords))
                .filter(word => word != null)
                .join(' ')
                .toLowerCase();
            console.log(`  Cleaned text : ${allTexts}`)
            const gamesWithRating = snesData.map(game => ({
                game,
                score: stringSimilarity
                    .compareTwoStrings(allTexts, game.name.toLowerCase())
            }));
            const gamesFound = gamesWithRating
                .sort((game1, game2) => game2.score - game1.score)
                .filter(x => x.score > toleranceInTitle);

            if (gamesFound.length === 0) {
                console.log('    Game was not found.');
            } else {
                const gameFound = gamesFound[0];
                console.log(`    ${gameFound.game.name} made by ${gameFound.game.company} in ${gameFound.game.year} (${gameFound.score})`);    
            }

            
        } else {
            console.log('SNES logo not found in image.');
        }
    }
}

async function isSnesLogoPresent(imagePath) {
    const logos = await findLogos(imagePath);
    return logos.indexOf('Super Nintendo Entertainment System') > -1;
}

main();